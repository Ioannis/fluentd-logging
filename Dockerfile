FROM fluent/fluentd:v1.7.2-debian-1.0

USER root

RUN buildDeps="sudo make gcc g++ libc-dev" \
        && apt-get update \
        && apt-get install -y --no-install-recommends $buildDeps \
        && apt-get install -y --no-install-recommends curl \
    && mkdir /var/lib/fluentd && chmod -R 777 /var/lib/fluentd /var/log/ \
    #&& apt-get install -y --no-install-recommends curl python3 python3-pip \
    #    && if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi \
    #    && pip3 install requests \
    # cutomize following instruction as you wish
    && sudo gem install fluent-plugin-rewrite-tag-filter \
    fluent-plugin-grok-parser \
    fluent-plugin-record-modifier \
    fluent-plugin-viaq_data_model \
    && sudo gem sources --clear-all \
    && SUDO_FORCE_REMOVE=yes \
    apt-get purge -y --auto-remove \
                  -o APT::AutoRemove::RecommendsImportant=false \
                  $buildDeps \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

USER fluent