#!/bin/bash

# a script to send fluentd events from a buffer file to Central Monitoring
# using HTTP submission (cf. http://monitdocs.web.cern.ch/monitdocs/ingestion/service_logs.html#http)
# The fluentd buffer file contains json-formatted events, one per line.
# All we need is to merge them into a json array and submit them to the Central Monitoring HTTP endpoint.

matomo_endpoint=${MATOMO_HTTP_ENDPOINT:-https://test-matomo-statistics-api.web.cern.ch/piwik.php}
buffer_file=$1

# use a small ruby script (fluentd itself is Ruby) to send logs to Matomo HTTP endpoint in a streaming way.
# it is important to keep the "-o /dev/null", otherwise fluentd considers that curl fails (even though it succeeds) and keeps sending the same log lines until the buffer file gets purged
ruby /fluentd/sendmatomo.rb $buffer_file | \
    curl --silent --fail -H 'Content-Type: application/json; charset=UTF-8' --data-binary @- "${matomo_endpoint}" -o /dev/null