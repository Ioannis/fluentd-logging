require 'json'
require 'uri'
require 'yaml'

#the script sends logs in a streaming way. It builds the request line by line and pipes it to curl.
begin
print '{"token_auth":"'
print ENV["MATOMO_API_TOKEN_AUTH"]
print '","requests":['
firstline = true
    while gets do
        fields = JSON.parse($_.chomp)
        lookup = YAML.load_file('/fluentd/idmapping/lookup.yaml')
        idsite = lookup["#{fields["request_header_host"]}"]
        next if not idsite
        if not firstline #all lines except the first
            print ','
        end
        q = '?' + URI.encode_www_form("idsite" => idsite, "url" => "http://#{fields["url"]}", "cip" => "#{fields["cip"]}", "rec" => "1")
        print q.to_json
        firstline = false
    end
print ']}'
end