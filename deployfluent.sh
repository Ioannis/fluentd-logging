#!/bin/bash

oc create serviceaccount fluentd-logging -n fluentd-logging
oc label serviceaccount fluentd-logging -n fluentd-logging app="fluentd-logging"
oc annotate serviceaccount fluentd-logging -n fluentd-logging description="Used by logging-fluentd daemonset to collect haproxy logs and send them to matomo and central monitoring"
oc adm policy add-cluster-role-to-user cluster-reader system:serviceaccount:fluentd-logging:fluentd-logging
oc create -n fluentd-logging configmap logging-fluentd --from-file=./fluent.conf
oc create -n fluentd-logging configmap logging-fluentd-scripts --from-file=./scripts
oc create -n fluentd-logging configmap custom-patterns --from-file=./grok/
oc create -n fluentd-logging configmap idmapping
oc label configmap/logging-fluentd configmap/logging-fluentd-scripts configmap/custom-patterns configmap/idmapping -n fluentd-logging app="fluentd-logging"
oc process --local -f ./deploy/logging-fluentd-template.yaml --param-file=./deploy/parameters/dev | oc create -n fluentd-logging -f -
